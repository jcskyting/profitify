# README

## init database
docker run -d \
    --rm \
    -p 5432:5432 \
    --name profitify-postgres \
    -e POSTGRES_PASSWORD=1234 \
    -e PGDATA=/var/lib/postgresql/data/pgdata \
    -v /Users/sky/project/profitify/pgdata:/var/lib/postgresql/data \
    -v /var/run/postgresql:/var/run/postgresql \
    postgres